import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

const Person = ({ img: imageNum, name, job, children }) => {
  const imageUrl = `https://randomuser.me/api/portraits/thumb/men/${imageNum}.jpg`;
  return (
    <article className="person">
      <img src={imageUrl} alt="person" />
      <h4>{name}</h4>
      <h4>{job}</h4>
      {children}
    </article>
  );
};

const PersonList = () => {
  return (
    <section className="person-list">
      <Person
        img={Math.floor(Math.random() * 100)}
        name="John"
        job="Developer"
      />
      <Person img={Math.floor(Math.random() * 100)} name="Bob" job="Scientist">
        <p> Great scientist from the ISRO. WHat a great man!!!</p>
      </Person>
      <Person
        img={Math.floor(Math.random() * 100)}
        name="David"
        job="Architect"
      />
    </section>
  );
};
ReactDOM.render(<PersonList />, document.getElementById("root"));
